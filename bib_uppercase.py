import re

#Reading in the original bibtex file, and getting all the titles
text = open("bib.txt").read()
titles = re.compile('title=\{(.*)\}')
titles = titles.findall(text)

#Wrapping {} around the uppercase characters
new_titles = []
for i in titles:
    new_title = ''
    for j in i:
        if j.lower() != j and j != i[0]:
            j = j.replace(j, '{'+j+'}')
        new_title += j
    new_titles.append(new_title)

#putting the new titles back
for i in range(0,len(titles)):
    text = re.sub(titles[i], new_titles[i], text)

#Writing it out
out = open('new_bib.bib', 'w')
out.write(text)

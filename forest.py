import time
import string
import random
from pprint import pprint

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


# The forest has the plants in it
class Forest():
    def __init__(self):
        self.plants = []

# The forest can magically create plants
    def create_plants(self, num_plants):
        for i in range(0, num_plants):
            x = Plant(id_generator())
            self.plants.append(x)

# The forest handles its own plants
# It increments every plants age and energy
# It also make the plants procreate
    def handle_plants(self):
        for plant in self.plants:
            plant.increment_age()
            print plant
            new_plant = plant.procreate()
            if new_plant != None:
                self.plants.append(new_plant)
            time.sleep(0.5)
            if plant.alive == False:
                print plant.name+" is dead"
                self.plants.remove(plant)

# Printing method for the forest
    def __str__(self):
        return "NUMBER OF TREES %s" % (len(self.plants))


        

# A plant has a boolean if its alive or dead
# It has a name, age, number of leaves and energy level
# The number of leaves is currently not used
class Plant():
    def __init__(self, name):
        self.alive = True
        self.name = name
        self.age = 0
        self.leaves = 0
        self.energy = 0

    # Method for priting your state
    def __str__(self):
        return "%s AGE %s %s ENERGY %s" % (self.name, self.age, self.alive, self.energy)

    # Increment your age and energy
    def increment_age(self):
        self.age = self.age+1
        self.energy = self.energy + 1 
        self.set_alive()

    # If True alive otherwise your dead
    def set_alive(self):
        if self.age >= 10:
            self.alive = False
        else:
            self.alive = True

    # give birth to a new plant
    def procreate(self):
        if self.energy >= 5:
            self.energy = self.energy - 3
            return Plant(id_generator())
        else:
            pass

           
forest = Forest()
turn = 0
forest.create_plants(1)
while True:
    print
    turn += 1
    print 
    forest.handle_plants()
    print 
    print "TURN:", turn
    print forest
    print
    if len(forest.plants) == 0:
        break
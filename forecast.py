from dateutil import parser
import csv
import numpy as np
from pprint import pprint
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import normalize
from sklearn.linear_model import RidgeCV
from sklearn.metrics import mean_squared_error
from sklearn.svm import SVR
from theanets import Experiment
from theanets import Regressor
import pickle

def writeCSV():
    data_out = open('data.csv','wb')
    with open('forecast_2011_2012.txt', 'rb') as csvfile:
        rows = csvfile.readlines()
        for i in rows:
            print i
            time = i.split('\t')[0].replace('"','')
            time = parser.parse(time)
            spamwriter = csv.writer(data_out, delimiter=',')
            spamwriter.writerow([time.weekday(), time.day,  time.month, time.hour, i.split('\t')[1].replace('\n','')])

def getDataSet(aggregate):
    with open('forecast_2011_2012.txt', 'rb') as csvfile:
        rows = csvfile.readlines()
        data = []
        target = []
        for i in rows:
            time = i.split('\t')[0].replace('"','')
            time = parser.parse(time)

            if aggregate == True:
                #aggregating hours
                morning = 1 if time.hour < 9 else 0
                midday = 1 if time.hour >= 9 and time.hour < 16 else 0
                evening = 1 if time.hour >= 16 and time.hour <= 21 else 0
                late = 1 if time.hour > 21 else 0

                #aggregating months
                winter = 1 if time.month == 12 or time.month == 1 or time.month == 2 else 0
                spring = 1 if time.month == 3 or time.month == 4 or time.month == 5 else 0
                summer = 1 if time.month == 6 or time.month == 7 or time.month == 8 else 0
                autumn = 1 if time.month == 9 or time.month == 10 or time.month == 11 else 0

                data.append([time.weekday(), time.day, time.month, time.hour,
                            morning, midday, evening, late, winter, spring, summer, autumn])
            else:
                data.append([time.weekday(), time.day, time.month, time.hour])

            target.append(int(i.split('\t')[1].replace('\n','')))

    X = np.array([el for el in data])
    Y = np.array([el for el in target])
    print X
    return X, Y


def train(X, Y, model):
    print "training"
    print
    if model == "ridge":
        clf = RidgeCV(normalize=True)
        model = clf.fit(X, Y)
        y_predicted = model.predict(X)
        print model
        print "MSE", mean_squared_error(Y, y_predicted)
        print
    if model == "svm":
        clf = SVR(verbose=True)
        model = clf.fit(X, Y)
        y_predicted = model.predict(X)
        print model
        print "MSE", mean_squared_error(Y, y_predicted)
        print

#doesnt work at the moment
    if model == "neural":
        dataset = (X, Y)
        input_layer = 90
        hidden1 = 30
        output_layer = 1
        layers = {}
        layers['input'] = input_layer
        layers['hidden'] = hidden1
        layers['output'] = output_layer

        e = Experiment(Regressor,
                       layers=(input_layer, hidden1, output_layer),
                       num_updates=23,
                       optimize='sgd')

        e.run(dataset, dataset)
        e.save('network.dat')
        layers_out = open('layers.dat', 'w')
        pickle.dump(layers, layers_out)

        mse = 0
        for i in range(0, len(X)):
            predicted = e.predict(X[i])
            golden = Y[i]
            mse += (predicted-golden)**2
        print "MSE", float(mse)/len(X)

def main():
    print "Building data set"
    X, Y = getDataSet(True)
    print "encoding"
    encoder = OneHotEncoder()
    X = encoder.fit_transform(X).todense()
    print X.shape, Y.shape

    #train(X, Y, "ridge")
    #train(X, Y, "svm")
    train(X, Y, "neural")


if __name__ == '__main__':
    main()
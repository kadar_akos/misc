from glob import glob
from os import getcwd, chdir
from PIL import Image
from subprocess import call
import time


#Put me in a folder with pictures and descriptions.txt
#The descriptions should describe the pictures line-by-line in order
#One description per line that is
#Give the pictures and the descriptions, I generate a tex file
#If you compile it, it becomes a beamer presentation of the pictures and descriptions

#Compressing the pictures, deleting the originals
def compress(format):
    path = getcwd()
    chdir(path)
    counter = 0
    pictures = glob('*.'+format)
    for i in pictures:
        counter += 1
        im = Image.open(i)
        im.save(i.lower(), quality=50)
        call(["rm", i])


#Creating the body of the tex file, with figures
def include_pictures(scale, format):
    descriptions_in = open('descriptions.txt')
    descriptions = descriptions_in.readlines()
    path = getcwd()
    chdir(path)
    pictures = glob('*.'+format)
    pictures = sorted(pictures, key=lambda x: int(x.split('_')[1].split('.')[0]))
    print pictures
    body = ''
    for i in range (0, len(pictures)):
        a = '\n'
        b = '\\begin{frame}'+'\n'
        c = '\\begin{figure}'+'\n'
        d = '\centering'+'\n'
        e = '\includegraphics'+'['+'scale'+'='+str(scale)+']'+'{'+pictures[i]+'}'+'\n'
        f = '\caption' +'{' + descriptions[i].strip() + '}'+'\n'
        g = '\end{figure}'+'\n'
        h = '\end{frame}'+'\n'
        i = '\n'
        body = body + a+b+c+d+e+f+g+h+i
    body += '\end{document}'
    return body

#saving out the tex file
def createTEX(document):
    latex_out = open('latex.tex', 'w')
    latex_out.write(document)
    latex_out.close()


#Giving commands to the OS to create the PDF, remove some files and open the resulting pdf
def toPDF():
    call(["pdflatex", "latex.tex"])
    call(["rm", "latex.aux", "latex.log", "latex.nav", "latex.out", "latex.snm", "latex.toc"])
    time.sleep(1.0)
    call(["gnome-open", "latex.pdf"])

format = 'JPG'
title = 'Babaval'
compress(format)
time.sleep(5.0)
header = open('header').read()
title = '\n'+'\\begin{frame}'+'\n'+'\\vspace{15mm}'+'\n'+'\centering'+'\n'+'{\Huge '+title+'}'+'\n'+'\end{frame}'+'\n'+'\n'
header = header + '\n' + '\\begin{document}' + '\n' + '\setbeamertemplate{caption}{\\raggedright\insertcaption\par}'+'\n'
body = include_pictures(0.1, format.lower())
document = header+title+body
createTEX(document)
time.sleep(1.0)
toPDF()
